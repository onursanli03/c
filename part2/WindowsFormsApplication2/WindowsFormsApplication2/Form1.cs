﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void chkInternet_CheckedChanged(object sender, EventArgs e)
        {
            if (chkInternet.Checked == true)
                this.Text = "İnternet seçildi";
            else
                this.Text = "Form Uygulaması";
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            //tıklandığında seçilen her kontrolün hangileri olduğunu belirtsin

            string yapilanSecimler = "";
            //form üzerindeki tüm kontrolleri dolaş 
            foreach (var control in this.Controls)
            {   
                //gezilen kontrollerden herhangi birisi CheckBox ise
                if (control is CheckBox)
                {
                    //bu CheckBoxlardan işaretli olan varsa
                    if (((CheckBox)control).Checked)
                       yapilanSecimler += ((CheckBox)control).Text + ", ";   
                    
                }

            }

            MessageBox.Show(string.Format("{0} tercihleriniz olarak işlendi", yapilanSecimler), "Seçilenler");
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //this.Text = listBox1.SelectedIndex.ToString(); //index bilgisi
            this.Text = listBox1.SelectedItem.ToString(); //görsel bilgi
            ListBox.SelectedIndexCollection secilenIndexler = listBox1.SelectedIndices;

            label2.Text = "";

            for (int i = 0; i < secilenIndexler.Count; i++)
            {
                label2.Text += secilenIndexler[i] + " ";

            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            rbKadin.Checked = true; //form açıldığında yapılacak işlemler burada yazılmalı.
        }

       

        private void rbErkek_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
