﻿namespace dataErisimi
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbKategoriler = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lbKategoriler
            // 
            this.lbKategoriler.FormattingEnabled = true;
            this.lbKategoriler.ItemHeight = 16;
            this.lbKategoriler.Location = new System.Drawing.Point(13, 26);
            this.lbKategoriler.Name = "lbKategoriler";
            this.lbKategoriler.Size = new System.Drawing.Size(95, 452);
            this.lbKategoriler.TabIndex = 0;
            this.lbKategoriler.SelectedIndexChanged += new System.EventHandler(this.lbKategoriler_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 699);
            this.Controls.Add(this.lbKategoriler);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lbKategoriler;
    }
}

