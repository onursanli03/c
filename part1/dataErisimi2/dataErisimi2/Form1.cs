﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dataErisimi2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            KategoriGetir();
        }
       
        SqlConnection con = new SqlConnection(@"server = SC-105-06\MSSQL2012;               database=northwnd; integrated security = sspi");
        
        void KategoriGetir()
        {
            SqlDataAdapter adp = new SqlDataAdapter("select CategoryID,CategoryName from Categories",con);

            DataTable dt = new DataTable();
            adp.Fill(dt);

            cbCategory.DisplayMember = "CategoryName";
            cbCategory.ValueMember = "CategoryID";
            cbCategory.DataSource = dt;
        }

        private void cbCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlDataAdapter adp = new SqlDataAdapter("select * from Products, Categories where Products.CategoryID = Categories.CategoryID and Categories.CategoryID = @catID ",con); //@catID sürekli değişim gösterecek ve bu cammande bir parametre olarak eklendi

            adp.SelectCommand.Parameters.AddWithValue("@catID",cbCategory.SelectedValue);//parametre bilgiyi nereden alıyor.

            DataTable dt = new DataTable();
            adp.Fill(dt);

            lbUrun.DisplayMember = "ProductName";
            lbUrun.ValueMember = "ProductID";
            lbUrun.DataSource = dt;


        }
    }
}
