﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dataErisimi3
{
    public partial class Urunler : Form
    {
        public Urunler()
        {
            InitializeComponent();
        }

SqlConnection con = new SqlConnection(@"server = SC-105-06\MSSQL2012; database=northwnd; integrated security = sspi");
       
        private void Urunler_Load(object sender, EventArgs e)
        {
            SqlDataAdapter adp = new SqlDataAdapter("select * from Products, Categories where Products.CategoryID = Categories.CategoryID and Categories.CategoryID = @id",con);

            adp.SelectCommand.Parameters.AddWithValue("@id", Form1.kategoriID);
            //bilgi diğer formdan geliyor.

            DataTable dt = new DataTable();
            adp.Fill(dt);
            dataGridView1.DataSource = dt;
        }
    }
}
