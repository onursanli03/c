﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kronometre
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            timer1.Start();
            btnStart.Visible = false;

        }
        int etap = 0;
        private void btnStop_Click(object sender, EventArgs e)
        {
            etap++;
            timer1.Stop();
            btnStart.Visible = true;

            listBox1.Items.Add(string.Format("{0}. Etap: {1}:{2}",etap,saniye,salise));
        }

        int saniye = 0, salise = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            salise++;
            if (salise == 60)
            {
                salise = 0;
                saniye++;
            }
            lblSalise.Text = salise.ToString();
            lblSaniye.Text = saniye.ToString();
        }
    }
}
