﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dataErisimi3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        SqlConnection con = new SqlConnection(@"server = SC-105-06\MSSQL2012; database=northwnd; integrated security = sspi");

        DataTable dt = new DataTable();

        public static int kategoriID;

        private void Form1_Load(object sender, EventArgs e)
        {
            SqlDataAdapter adp = new SqlDataAdapter("Select * from Categories", con);


            adp.Fill(dt);


            dataGridView1.DataSource = dt;
            dataGridView1.Columns["Picture"].Visible = false;
            dataGridView1.Columns["CategoryID"].Visible = false;
        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            kategoriID = (int)dt.Rows[e.RowIndex]["CategoryID"];

            Urunler frm = new Urunler();
            frm.ShowDialog();
        }
    }
}
