﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zamanlayici
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        //10 saniye içinde maximum tık sayısına ulaşıcaz ve oyunun sonunda tekrar oynayıp oynamayacağımız sorulsun.
        int sayac = 0;
        bool basladikMi = false;
        private void btnBisey_Click(object sender, EventArgs e)
        {
            sayac++;
            lblSayi.Text = sayac.ToString();
            if (!basladikMi)
            {
                timer1.Start();
                basladikMi = true;
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            btnBisey.Enabled = false;
          DialogResult cevap = MessageBox.Show(string.Format("{0} adet tıklama gerçekleştirdiniz. \nYeniden oynamak ister misin?", sayac),"Süre Doldu!...",MessageBoxButtons.YesNo,MessageBoxIcon.Question);

            //evet derse yeniden başlatalım
          if (cevap == DialogResult.Yes)
          {
              sayac = 0;
              lblSayi.Text = "0";
              btnBisey.Enabled = true;
          }
          else //hayır derse programı kapatalım.
          {
              Application.Exit();
          }
        }
    }
}
