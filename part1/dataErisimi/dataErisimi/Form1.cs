﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;//disconnected
using System.Data.SqlClient;//connected
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dataErisimi
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(@"server = SC-105-06\MSSQL2012; database=northwnd; integrated security = sspi"); // connection string hangi sunucuya, bu sunucuda hangi veritabanına hangi oturum bilgileriyle (login) bağlanacağımızı belirler

            SqlCommand secim = new SqlCommand("select * from Categories", con);
            con.Open();//komutların çalıştırılabilmesi açık bir bağlantıyı gerektirir.

            SqlDataReader dr = secim.ExecuteReader();

            while (dr.Read())//okunabilir bir kayıt ile karşılaşırsan (result set varsa)
            {
                lbKategoriler.Items.Add(dr[1]);
            }
        }

        private void lbKategoriler_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Text = lbKategoriler.SelectedItem.ToString();
        }
    }
}
